# Lists and Keys

Veja o exemplo a seguir que monta um lista de categorias:

```
function Categorias() {
    const categorias = ['Banheiro', 'Camas', 'Cozinha', 'Piscina', 'Jogos']
    return (
        <ul>
            {categorias.map(categoria => (
                <li>{categoria}</li>
            ))}
        </ul>
    );
}
```

Nesta função, usamos a função `map()` do JavaScript para iterarmos pelo array categorias e retornar o elemento `<li>` para cada item do array. Deste modo, criamos uma lista de categorias que pode ser usada em um menu ou um filtro de categoria.

Porém, ao executar esse código, você receberá um aviso que uma chave (`key`) deve ser definida para os itens da lista.
`key` é um atributo string especial que você precisa definir ao criar listas de elementos.

As `key's` ajudam o React a identificar quais itens sofreram alterações, foram adicionados ou removidos. Elas devem ser atribuídas aos elementos dentro do array para dar uma identidade estável aos elementos:

```
function Categorias() {
    const categorias = ['Banheiro', 'Camas', 'Cozinha', 'Piscina', 'Jogos']
    return (
        <ul>
            {categorias.map(categoria => (
                <li key={categoria.toString()}>{categoria}</li>
            ))}
        </ul>
    );
}
```

A melhor forma de escolher uma chave é usar uma string que identifica unicamente um item da lista dentre os demais. Na maioria das vezes você usaria IDs de seus dados como chave:

```
const todoItems = todos.map(todo =>
    <li key={todo.id}>
        {todo.text}
    </li>
)
```

Quando você não possui nenhum ID estável para os itens renderizados, você pode usar o índice do item como chave em último recurso:

```
const todoItems = todos.map((todo, index) =>
    <li key={index}>
        {todo.text}
    </li>
)
```

Não é recomendado o uso de índices para chave se a ordem dos itens pode ser alterada. Isso pode impactar de forma negativa o desempenho e poderá causar problemas com o estado do componente. 

### Documentação e Exemplos:
- [Lists and Keys](https://reactjs.org/docs/lists-and-keys.html)
- [map](https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Array/map)

# Forms

Os elementos de formulário HTML funcionam de maneira um pouco diferente de outros elementos DOM no React, porque os elementos de formulário mantêm naturalmente algum estado interno. Por exemplo, este formulário em HTML puro aceita um único nome:

```
<form>
    <label>
        Nome:
        <input type="text" name="nome" />
    </label>
    <input type="submit" value="Enviar" />
</form>
```

Esse formulário tem o comportamento padrão do HTML de navegar para uma nova página quando o usuário enviar o formulário. Se você quer esse comportamento no React, ele simplesmente funciona. Mas na maioria dos casos, é conveniente ter uma função Javascript que manipula o envio de um formulário e tem acesso aos dados que o usuário digitou nos inputs. O modo padrão de fazer isso é com uma técnica chamada “componentes controlados” (controlled components).

## Componentes Controlados (Controlled Components)

Em HTML, elementos de formulário como `<input>`, `<textarea>` e `<select>` normalmente mantêm seu próprio estado e o atualiza baseado na entrada do usuário. Em React, o estado mutável é normalmente mantido na propriedade state dos componentes e atualizado apenas com `setState()`.

Podemos combinar os dois fazendo o estado React ser a “única fonte da verdade”. Assim, o componente React que renderiza um formulário também controla o que acontece nesse formulário nas entradas subsequentes do usuário. Um input cujo o valor é controlado pelo React dessa maneira é chamado de “componente controlado” (controlled component).

Por exemplo, se quisermos que o exemplo anterior registre o nome quando ele for enviado, podemos escrever o formulário como um componente controlado:

```
class getCEP extends Component {
    state = {
        cep: ''
    }

    handleChange = event => {
        this.setState({ cep: event.target.value })
    }

    handleSubmit = event => {
        event.preventDefault()
        alert('O CEP foi enviado: ' + this.state.cep)
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <label>
                    CEP:
                    <input type="text" onChange={this.handleChange} />
                </label>
                <input type="submit" value="Enviar" />
            </form>
        )
    }
}
```

Como o atributo value é definido no nosso `<input type="text">`, o valor exibido sempre será o mesmo de this.state.value, fazendo com que o estado do React seja a fonte da verdade. Como o handleChange é executado a cada tecla pressionada para atualizar o estado do React, o valor exibido será atualizado conforme o usuário digita.

Com um componente controlado, cada mutação de estado terá uma função de manipulação (handler function) associada. Isso faz com que seja simples modificar ou validar a entrada do usuário.

No geral, as tags `<input type="text">`, `<textarea>` e `<select>` funcionam de forma muito semelhante - todas elas aceitam um atributo value que você pode usar para implementar um componente controlado.

### Documentação e Exemplos:
- [Forms](https://reactjs.org/docs/forms.html)
- [bind](https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Global_Objects/Function/bind)
- [event.preventDefault](https://developer.mozilla.org/pt-BR/docs/Web/API/Event/preventDefault)

# Lifting State Up (Elevar o State)

Elevar o State é usado quando queremos que um elemento pai seja a "fonte da verdade" de seus elementos filhos, ou seja, ele compartilha seu `state` através de `props` para seus elementos filhos, e estes podem mudar o state do elemento pai.

"No React, o compartilhamento do state é alcançado ao movê-lo para o elemento pai comum aos componentes que precisam dele".

Vejamos isso na prática:

Em nosso elemento pai temos um `state` uma função `changePrice` e três elementos filhos:
- Input: responsável por mudar o `state` pai
- NumberPrice: exibe o valor do `state.price`
- FormattedPrice: exibe o `state.price` formatado

```
class App extends Component {

    state = {
        price: 0
    }

    changePrice = value => {
        this.setState({ price: value })
    }

    render() {
        return (
            <div className="App">
                <Input changePrice={this.changePrice}/>
                <NumberPrice price={this.state.price}/>
                <FormattedPrice price={this.state.price} />				
            </div>
        )
    }
}
```

Ao passarmos via `props` para o elemento `Input` a função que muda o state, chamamos essa função no `onChange` do input atribuindo como valor o valor digitado (`event.target.value`). Note que a função que passamos pertence a classe `App`, e por isso, no evento `onChange` do elemento `Input`, o `setState` modifica o state do elemento pai (`App`), desse modo, os elemento `NumberPrice` e `FormattedPrice` também são atualizado, pois a única fonte da verdade é o elemento pai.

Vale lembrar que estamos usando uma `arrow function`, deste modo, ao passarmos essa função como props, o `this` ainda será a classe `App`.

* Veja esse exemplo funcionando na pasta `desafio`

### Documentação e Exemplos: 
- [Lifting State Up](https://reactjs.org/docs/lifting-state-up.html)
- [Arrowfunctions](https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Functions/Arrow_functions)

# Composition vs Inheritance (Composição vs Herança)

O React tem um poderoso modelo de composição, e por isso recomendamos o uso de composição ao invés de herança para reutilizar código entre componentes.

## Benefícios da composição:
Já que o conceito de composição é uma parte tão importante do que torna o React incrível de se trabalhar, vamos nos aprofundar um pouco nisso. Lembre-se que composição é apenas a combinação de funções simples para criar funções complexas. Se atente a estes dois conceitos:

- funções simples
- combinadas para criar outras funções

Composição é construir a partir de funções simples.

Vejamos um exemplo de composição no React. É Recomendado que esses componentes utilizem a prop especial `children` para passar os elementos filhos diretos para sua respectiva saída (isso permite outros componentes passar elementos filhos no próprio JSX):

```
const name = "FatecLab"

function User(props) {
    return (
        <div className='user'>
            {props.children}
        </div>
    );
}

function WelcomeDialog() {
    return (
        <User name={name}>
            <h1 className="Dialog-title">
                Bem-vindo {this.props.name}
            </h1>
            <p className="Dialog-message">
                Obrigado por visitar nossa loja!
            </p>
        </User>
    );
}
```

Qualquer conteúdo dentro da tag JSX do componente `<User>` vai ser passado ao componente User como prop children. Desde que User renderize a `{props.children}` dentro de uma `<div>`, os elementos serão renderizados no resultado final.

## Mas e a herança?

Herança é um princípio de orientação a objetos, que permite que classes compartilhem atributos e métodos, através de "heranças". Ela é usada na intenção de reaproveitar código ou comportamento generalizado ou especializar operações ou atributos.

Porém o uso de props e composição irá te dar toda flexibilidade que você precisa para customizar o comportamento e aparência dos componentes, de uma maneira explícita e segura. Lembre-se de que os componentes podem aceitar um número variável de props, incluindo valores primitivos, como int, array, boolean; assim como elementos Reacts e funções.

Tenha sempre em mente que a composição desempenha um papel essencial na construção de componentes em React.

### Documentação e Exemplos: 
- [Composition vs Inheritance](https://reactjs.org/docs/composition-vs-inheritance.html)
- [Composição em React](https://www.devpleno.com/composicao-de-componentes-em-reactjs/)

# Desafio

Neste desafio vocês deverão criar um formulário de contato que possua o seguintes campos:

- Nome
- Telefone
- E-mail
- Descrição

Este formulário deve ser um `Componente Controlado` que possua uma validação de e-mail, ou seja, o e-mail deve estar preenchido corretamente.
O campo telefone deve possuir apenas números, com no máximo 9 dígitos.
Lembre-se que os campos não podem ser vazios no momento do envio.
Caso as informações não estiverem preechidas corretamente ou vazias, deve-se exibir uma mensagem ao usuário.

Se as informações estiverem corretas, no envio, logo abaixo do formulário deve-se exibir as informações do contato e uma mensagem de que as informações foram enviadas com sucesso. Use as `key's` neste momento para exibir as informações.

Use composição para: 
- Criar um componente para o formulário.
- Criar um componente para exibir as informações do usuário.
- Criar um componente que recebe o formulário e os dados do usuário através da prop especial children (`this.props.children`).

Use elevação de state:
- O state deve ficar no elemento pai que recebe elementos filhos via `props.children`.
- O componente formulário deve alterar o state do elemento pai quando as informações forem "enviadas", fazendo com que as informações sejam exibidas pelo componente irmão.

Inicialização:

- De um Fork neste repositório.
- Agora você deve clonar o repositório localmente, há um botão azul "Clone" no seu repositório do GitLab, clique nele e use a URL com HTTPS.
- Agora localmente abra uma pasta e use o botão direito do Mouse para abrir o "Git Bash Here", com esse atalho você chegará na pasta que quer mais rapidamente pelo terminal.
- Use o comando `git clone url-copiada-do-gitlab` para que a estrutura de pastas do repositório seja clonada na sua pasta.
- Crie uma branch com `git checkout -b seu-nome/basico-componentes-ii`.
- Entre na pasta desafio com `cd desafio`.
- Execute o `npm install` para instalar as dependências do projeto.

Validação do email:

- Você pode usar uma [Expressão Regular](https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Guide/Regular_Expressions)

```
const email = 'email@email.com'
const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
if (re.test(email)) {
    console.log('O e-mail é válido')
} else {
    console.log('O e-mail não é válido')
}
```

Entrega:

- Assim que terminar dê `git push origin seu-nome/basico-componentes-ii`.
- Acesse o menu "Merge Requests", configure o "Target Branch" para o repositório original para que seu App seja avaliado e revisado e para que possamos te dar um feedback.
- O nome do Merge Request deve ser o seu nome completo.
- Crie o Merge Request (MR).

Para se destacar na execução:
- Use um regex para validação do campo telefone.
- Personalize a página com CSS.
- Use responsividade para dispositivos Mobile.
- Crie uma pasta `components` para seus subcomponentes
- Utilize os nomes dos componentes que criar como o nome dos arquivos.