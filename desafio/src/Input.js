import React, { Component } from 'react'

class Input extends Component {

    render() {
        return <input type="number" onChange={event => {
            this.props.changePrice(event.target.value)
        }}/>
    }

}

export default Input